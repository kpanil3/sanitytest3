package Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Base.BaseClass;
import Base.BaseClass2;
import Pages.Dashboard;
import Pages.LoginPage;
import Utils.Utilities;

public class LoginTestWithTestData extends BaseClass2 {
	
	LoginPage lp;
	Dashboard db;
	Utilities utl;
	
	@DataProvider(name="userAndPassword")
	public String[][] userAndPwd(){
		
		String[][] userDetails = {{"1","2"}, {"a", "b"}, {"c", "d"}};
		return userDetails;
		
	}
	
	
	@BeforeClass
	public void setup() throws IOException {	

		FileInputStream fis = new FileInputStream(".\\Configuration\\Config.properties");
		Properties prop = new Properties();
		prop.load(fis);
		launchURL(prop.getProperty("browser"), prop.getProperty("url"));
		lp = new LoginPage(driver);
		db = new Dashboard(driver);
		utl = new Utilities(driver);
		
	}
	
	@Test
	public void checkLoginValidUser() {
		
		lp.login("Admin", "admin123");
		Assert.assertEquals(db.getHeader(), "Dashboard");
	}
	
	@Test (dataProvider ="userAndPassword")
	public void checkLoginInvalidUser(String username, String password) {
		lp.login("a", "b");
		Assert.assertEquals(lp.invalidMessage(), "Invalid credentials");
	}
	
	
	
	@AfterMethod
	
	public void failedTestCases(ITestResult result) throws IOException {
		if (result.FAILURE==result.getStatus()) {
			utl.takeScreenshot(result.getName());
		}
	}
	
	@AfterClass
	public void close() {
		
		driver.close();
	}
	

}
