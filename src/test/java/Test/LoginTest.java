package Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Base.BaseClass;
import Base.BaseClass2;
import Pages.Dashboard;
import Pages.LoginPage;
import Utils.Utilities;

public class LoginTest extends BaseClass2 {
	
	LoginPage lp;
	Dashboard db;
	Utilities utl;
	
	
	@BeforeClass(alwaysRun=true)
	public void setup() throws IOException {	

		FileInputStream fis = new FileInputStream(".\\Configuration\\Config.properties");
		Properties prop = new Properties();
		prop.load(fis);
		launchURL(prop.getProperty("browser"), prop.getProperty("url"));
		lp = new LoginPage(driver);
		db = new Dashboard(driver);
		utl = new Utilities(driver);
		
	}
	
	@Test (groups= {"Sanity"})
	public void checkLoginValidUser() {
		
		lp.login("Admin", "admin123");
		Assert.assertEquals(db.getHeader(), "Dashboard");
	}
	
	@Test (groups= {"Sanity"})
	public void checkLoginInvalidUser() {
		lp.login("a", "b");
		Assert.assertEquals(lp.invalidMessage(), "Invalid credentials");
	}
	@AfterMethod(alwaysRun=true)
	
	public void failedTestCases(ITestResult result) throws IOException {
		if (result.FAILURE==result.getStatus()) {
			utl.takeScreenshot(result.getName());
		}
	}
	
	@AfterClass(alwaysRun=true)
	public void close() {
		
		driver.close();
	}
	

}
