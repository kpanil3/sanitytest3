package Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Base.BaseClass;
import Pages.Dashboard;
import Pages.LoginPage;
import Utils.Utilities;

public class LoginTestInParallel extends BaseClass {
	
	LoginPage lp;
	Dashboard db;
	Utilities utl;
	LoginPage lp1;
	Dashboard db1;
	Utilities utl1;
		public void setup1_forChrome() throws IOException {	

		lp = new LoginPage(driver_chrome);
		db = new Dashboard(driver_chrome);
		utl = new Utilities(driver_chrome);
		
	}
		
		public void setup2_forFirefox() throws IOException {	

		lp1 = new LoginPage(driver_firefox);
		db1 = new Dashboard(driver_firefox);
		utl1 = new Utilities(driver_firefox);
		
	}
	
	

	@Test (priority=1)
	@Parameters({"browser1", "url"})
	public void checkLoginValidUser(String browser1, String url) throws IOException {
		launchURL(browser1, url);
		setup1_forChrome();
		lp.login("Admin", "admin123");
		Assert.assertEquals(db.getHeader(), "Dashboard");
		driver_chrome.close();
	}
	
	@Test (priority=2)
	@Parameters({"browser2", "url"})
	public void checkLoginInvalidUser(String browser2, String url) throws IOException {
		launchURL(browser2, url);
		setup2_forFirefox();
		lp1.login("a", "b");
		Assert.assertEquals(lp1.invalidMessage(), "Invalid credentials");
		driver_firefox.close();
	}
	
	
	@AfterMethod(alwaysRun=true)
	
	public void failedTestCases(ITestResult result) throws IOException {
		if (result.FAILURE==result.getStatus()) {
			utl.takeScreenshot(result.getName());
		}

	}
	
	

}
