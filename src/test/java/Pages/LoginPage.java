package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id ="txtUsername") WebElement username;
	@FindBy(id = "txtPassword") WebElement password;	
	@FindBy(id = "btnLogin") WebElement btnLogin;
	@FindBy(id ="spanMessage") WebElement invalidLogin;
	
	public void login(String uname, String passwd) {
		
		username.sendKeys(uname);
		password.sendKeys(passwd);
		btnLogin.click();
		
	}
	
	public String invalidMessage() {
		return invalidLogin.getText();
	}
	
	

}
