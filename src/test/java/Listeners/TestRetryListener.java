package Listeners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class TestRetryListener implements IRetryAnalyzer{
    int counter = 0;
    private final static int maxRetryCount = 3;
	public boolean retry(ITestResult result) {
		if (counter<maxRetryCount) {
			counter++;
			return true;
		}
		return false;
	}
	

	}


