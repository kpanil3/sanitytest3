package Base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseClass {

	public WebDriver driver_chrome ;
	public WebDriver driver_firefox ;


	public void launchURL(String browser, String url) {

		if (browser.equalsIgnoreCase("Chrome")) {

			WebDriverManager.chromedriver().setup();
			driver_chrome = new ChromeDriver();
			driver_chrome.get(url);
			driver_chrome.manage().window().maximize();
			driver_chrome.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}

		if (browser.equalsIgnoreCase("firefox")) {

			WebDriverManager.firefoxdriver().setup();
			driver_firefox = new FirefoxDriver();
			driver_firefox.get(url);
			driver_firefox.manage().window().maximize();
			driver_firefox.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		else
		{
			System.out.println("Please enter either chrome or Firefox");
		}
		
		
	}
	

}
