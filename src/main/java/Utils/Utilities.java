package Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Utilities {
	
    WebDriver driver; 
    
    public Utilities(WebDriver driver) {
    	
    	this.driver = driver;
    	
    }
	public void takeScreenshot(String fileName) throws IOException {
    	  
    	  File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    	  File destFile = new File(".//Screenshots//"+fileName+".jpg");
    	  FileUtils.copyFile(srcFile, destFile);
    	  
      }
}
